*** Settings ***
Documentation		Test Login Activity in Payfazz
Library					AppiumLibrary
Suite Setup			Open Application Calculator
Suite Teardown	Close Application

*** Variables ***
${DEV.APPIUM_SERVER}		http://localhost:4723/wd/hub
${DEV.PLATFORM_NAME}		Android
${DEV.PLATFORM_VERSION}	5.0.2
${DEV.DEVICE_NAME}			5add21a2
${DEV.PACKAGE_NAME}			com.sec.android.app.popupcalculator
${DEV.ACTIVITY_NAME}		com.sec.android.app.popupcalculator.Calculator

// Element Locators
${EQUAL_SIGN}           xpath=//*[contains(@text, '=')]
${DISPLAYED_RESULT}     xpath=//*[contains(@resource-id, 'id/txtCalc')]

// Data
${td_digit1}            5
${td_digit2}            2
//${td_Expected_Addition_Result}    7
//${td_Expected_Subtraction_Result} 3

*** Test Cases ***
Additional
    Enter Digit and Operator    ${td_digit1}  ${td_digit2}    +
    View Result

*** Keywords ***
Open Application Calculator
		[Documentation]			Open Calculator App
		Open Application		${DEV.APPIUM_SERVER}  platformName=${DEV.PLATFORM_NAME}	platformVersion=${DEV.PLATFORM_VERSION}		deviceName=${DEV.DEVICE_NAME}		appPackage=${DEV.PACKAGE_NAME}	appActivity=${DEV.ACTIVITY_NAME}		autoGrantPermissions=true

Enter Digit and Operator
    [Arguments]     ${digit1}   ${digit2}   ${operator}
    Click Element   xpath=//*[contains(@text, '5')]
    Click Element   xpath=//*[contains(@text, '+')]
    Click Element   xpath=//*[contains(@text, '3')]

View Result
    Click Element   ${EQUAL_SIGN}

*** Settings ***
Library		AppiumLibrary


*** Test Cases ***
Application should displayed when open app
		Open Application			http://localhost:4723/wd/hub
		platformName					Android
		platformVersion				5.0.2
		deviceName						5add21a2
		app										${CURDIR}/apk/payfazz.apk
		automationName				appium
		appPackage						com.payfazz.android

HomePage should displayed when login using valid data
		Button Login
		Phone Number					087784292693
		Button Lanjut
    OTP                   {OTP}
    Button Lanjut
		Password							@Faiz2693
		Button Masuk
		HomePage should displayed

*** Keywords ***
Button Login
		Click Button					xpath=//android.widget.Button[contains(@resource-id,'button_register_onboarding')]

Phone Number
		[Arguments]						${phone}
		Input Text						id=com.payfazz.android:id/edit_text_phone		${phone}

Button Lanjut
		Click button					xpath=//android.widget.Button[@text='LANJUT' and @index='0']

Password
		[Arguments]						${password}
		Input Text						id=com.payfazz.android:id/edit_text_password	${password}

Button Masuk
		Click Button								xpath=//android.widget.Button[@text='MASUK' and @index='0']

HomePage should displayed
		Element Should Displayed		id=com.payfazz.android:id/image_view_icon

*** Settings ***
Documentation		Test Login Activity in Payfazz
Library					AppiumLibrary
Suite Setup			Open Application Payfazz
Suite Teardown	Close Application

*** Variables ***
${DEV.APPIUM_SERVER}		http://localhost:4723/wd/hub
${DEV.PLATFORM_NAME}		Android
${DEV.PLATFORM_VERSION}	5.0.2
${DEV.DEVICE_NAME}			5add21a2
${DEV.PACKAGE_NAME}			com.payfazz.android.debug
${DEV.ACTIVITY_NAME}		com.payfazz.android.user.authorize.presentation.activity.LoginActivity

*** Test Cases ***
Empty Phone and Password Field
    Phone Number					${EMPTY}
    sleep									1s
    Button Lanjut Should Disabled

Empty Phone but Fill Password
    Phone Number					${EMPTY}
    sleep									1s
    Button Lanjut
    Password              ${Data_Password}
    Button Masuk
    Error Message Should Displayed

Empty Password but Fill Phone
    Phone Number					${Data_Phone}
    sleep									1s
    Button Lanjut
    Password              ${EMPTY}
    Button Masuk

*** Keywords ***
Open Application Payfazz
    [Documentation]			Open Payfazz App
    Open Application		${DEV.APPIUM_SERVER}  platformName=${DEV.PLATFORM_NAME}	platformVersion=${DEV.PLATFORM_VERSION}		deviceName=${DEV.DEVICE_NAME}		appPackage=${DEV.PACKAGE_NAME}	appActivity=${DEV.ACTIVITY_NAME}		autoGrantPermissions=true

Button Login
    [Documentation]			Click Login Registered User
    Click Text					xpath = //*[contains(@text, '${Sudah Punya Akun? Login Disini}')]

Phone Number
    [Documentation]			Fill Phone Number
    [Arguments]					${phone}
    Input Text					xpath=//*[contains(@resource-id, 'id/edit_text_phone')]		${phone}

Button Lanjut
    [Documentation]			Click Button Lanjut
    Click Element				xpath=//*[contains(@text, 'LANJUT')]

Password
    [Documentation]			Fill Secured Password
    [Arguments]					${password}
    Input Text					xpath=//*[contains(@resource-id, 'id/edit_text_password')]	${password}

Button Masuk
    [Documentation]			Click Button Masuk
    Click Element				xpath=//*[contains(@text, 'MASUK')]

Error Message Should Displayed
    [Documentation]			Notification Error Messages
    Element Text Should Be     xpath=//*[contains(@resource-id, 'id/textinput_error')]    No hp atau password salah

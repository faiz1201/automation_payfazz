// Use LANG environment variable to choose locale
pref("intl.locale.matchOS", true);

// Enable Network Manager integration
pref("network.manage-offline-status", true);
